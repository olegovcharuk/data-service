package com.electrictravel.dataservice.station;

import org.springframework.data.annotation.Id;

public class Station {

    @Id
    private String key;

    private String title;
    private String direction;
    private String country;
    private String region;
    private String settlement;
    private String stationType;
    private Double latitude;
    private Double longitude;

    public Station() {
    }

    public Station(
            String key,
            String title,
            String direction,
            String country,
            String region,
            String settlement,
            String stationType,
            Double latitude,
            Double longitude
    ) {
        this.key = key;
        this.title = title;
        this.direction = direction;
        this.country = country;
        this.region = region;
        this.settlement = settlement;
        this.stationType = stationType;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getSettlement() {
        return settlement;
    }

    public void setSettlement(String settlement) {
        this.settlement = settlement;
    }

    public String getStationType() {
        return stationType;
    }

    public void setStationType(String stationType) {
        this.stationType = stationType;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    @Override
    public String toString() {
        return "Station{" +
                "key='" + key + '\'' +
                ", title='" + title + '\'' +
                ", direction='" + direction + '\'' +
                ", country='" + country + '\'' +
                ", region='" + region + '\'' +
                ", settlement='" + settlement + '\'' +
                ", station_type='" + stationType + '\'' +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                '}';
    }
}
