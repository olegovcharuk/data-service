package com.electrictravel.dataservice.station;

import com.electrictravel.dataservice.settlement.Settlement;
import com.electrictravel.dataservice.settlement.SettlementRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/station")
public class StationController {

    private final StationRepository stationRepository;
    private final SettlementRepository settlementRepository;

    @Autowired
    public StationController(StationRepository stationRepository,
                             SettlementRepository settlementRepository) {
        this.stationRepository = stationRepository;
        this.settlementRepository = settlementRepository;
    }

    @RequestMapping(value = "/{key}", method = RequestMethod.GET)
    public Station findStationByKey(@PathVariable String key) {
        return stationRepository.findByKey(key);
    }

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public List<Station> findAllStations() {
        return stationRepository.findAll();
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public void addStation(@RequestBody Station station) {
        stationRepository.save(station);
        if (!station.getSettlement().equals("")) {
            settlementRepository.save(new Settlement(station.getSettlement()));
        }
    }

    @RequestMapping(value = "/add/all", method = RequestMethod.POST)
    public void addAllStations(@RequestBody List<Station> stations) {
        stationRepository.deleteAll();
        settlementRepository.deleteAll();
        stations.forEach(station -> {
            stationRepository.save(station);
            if (!station.getSettlement().equals("")) {
                settlementRepository.save(new Settlement(station.getSettlement()));
            }
        });
    }
}
