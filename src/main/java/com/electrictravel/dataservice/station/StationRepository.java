package com.electrictravel.dataservice.station;

import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface StationRepository extends MongoRepository<Station, String> {
    Station findByKey(String key);
    List<Station> findAllBySettlement(String settlement);

    List<Station> findAllBySettlementAndStationType(String settlement, String stationType);
}
