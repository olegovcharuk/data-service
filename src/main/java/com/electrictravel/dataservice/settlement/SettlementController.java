package com.electrictravel.dataservice.settlement;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

@RestController
@RequestMapping("/settlement")
public class SettlementController {

    private final SettlementRepository settlementRepository;

    @Autowired
    public SettlementController(SettlementRepository settlementRepository) {
        this.settlementRepository = settlementRepository;
    }

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public List<Settlement> findAllSettlements(HttpServletResponse response) {
        response.setHeader("Access-Control-Allow-Origin", "*");
        return settlementRepository.findAll();
    }
}
