package com.electrictravel.dataservice.settlement;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface SettlementRepository extends MongoRepository<Settlement, String> {

}
