package com.electrictravel.dataservice.connection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/connection")
public class ConnectionController {

    private ConnectionRepository repository;

    @Autowired
    public ConnectionController(ConnectionRepository repository) {
        this.repository = repository;
    }

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public List<Connection> findAllConnections() {
        return repository.findAll();
    }

    @RequestMapping(value = "/add/all", method = RequestMethod.POST)
    public void addAllConnections(@RequestBody List<Connection> connections) {
        repository.deleteAll();
        repository.save(connections);
    }
}
