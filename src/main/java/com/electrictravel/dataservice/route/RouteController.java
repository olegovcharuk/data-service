package com.electrictravel.dataservice.route;

import com.electrictravel.dataservice.station.Station;
import com.electrictravel.dataservice.station.StationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpMethod;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletResponse;
import java.util.LinkedList;
import java.util.List;

@RestController
@RequestMapping("/route")
public class RouteController {

    private final String resolvingServiceUrl;
    private final StationRepository stationRepository;

    @Autowired
    public RouteController(StationRepository stationRepository,
                           Environment env) {
        this.stationRepository = stationRepository;
        this.resolvingServiceUrl = env.getProperty("resolving-service.url");
    }

    @RequestMapping(value = "/from/{from}/to/{to}")
    public List<Station> findRouteBetweenStations(
            @PathVariable("from") String settlementFrom,
            @PathVariable("to") String settlementTo,
            HttpServletResponse response
    ) {
        List<String> stationsFrom = findBestStationsBySettlement(settlementFrom);
        List<String> stationsTo = findBestStationsBySettlement(settlementTo);
        List<String> stationKeys =
                new RestTemplate()
                        .exchange(this.resolvingServiceUrl + "/route/from/{from}/to/{to}",
                                HttpMethod.GET,
                                null,
                                new ParameterizedTypeReference<List<String>>() {
                                },
                                stationsFrom, stationsTo)
                        .getBody();
        List<Station> stations = new LinkedList<>();
        stationKeys.forEach(key -> {
            stations.add(stationRepository.findByKey(key));
        });

        response.setHeader("Access-Control-Allow-Origin", "*");

        return stations;
    }

    private List<String> findBestStationsBySettlement(String settlement) {
        List<String> result = new LinkedList<>();
        List<Station> stations = stationRepository.findAllBySettlementAndStationType(settlement, "train_station");
        if (stations.size() != 0) {
            stations.forEach(station -> result.add(station.getKey()));
            return result;
        }
        stations = stationRepository.findAllBySettlementAndStationType(settlement, "station");
        if (stations.size() != 0) {
            stations.forEach(station -> result.add(station.getKey()));
            return result;
        }
        stations = stationRepository.findAllBySettlementAndStationType(settlement, "platform");
        if (stations.size() != 0) {
            stations.forEach(station -> result.add(station.getKey()));
            return result;
        }
        stations = stationRepository.findAllBySettlementAndStationType(settlement, "stop");
        if (stations.size() != 0) {
            stations.forEach(station -> result.add(station.getKey()));
            return result;
        }
        return result;
    }
}
